---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Techniques informatique Drummondville"
  text: ""
  tagline: 
  actions:
    - theme: brand
      text: Site de la technique sur le portail
      link: https://www.cegepdrummond.ca/programme/techniques-de-linformatique/
    

features:
  - title: Tes études
    details: Informations utiles pour tes études
    link: /actuel/
    linkText: y allez
    icon: { src: /logoetudiant.svg} 
  - title: Ton stage
    details: Ce que tu dois savoir pour faire ton stage de fin d'études
    link: /stages/
    linkText: y allez
    icon: { src: /logotravail.svg} 
  - title: International
    details: Tu viens d'arriver au Québec pour tes études?
    link: /international/
    linkText: y allez
    icon: { src: /logointernational.svg} 
  - title: Stages ATE
    details: Les stages d'été entre les sessions
    link: /stagesATE/
    linkText: y allez
    icon: { src: /logoATE.svg} 
  - title: Ton portable
    details: Comment bien utiliser ton portable
    link: /portable/
    linkText: y allez
    icon: { src: /logoportable.svg}
  - title: Ton code
    details: Les bonnes pratiques pour coder
    link: /programmation/
    linkText: y allez
    icon: { src: /logoclavier.svg}
  - title: Le plagiat
    details: En quoi consiste le plagiat 
    link: /plagiat/
    linkText: y allez
    icon: { src: /logoplagiat.svg} 
  - title: Ton social
    details: Les activités sociales 
    link: /actuel/cati
    linkText: y allez
    icon: { src: /logosocial.svg} 
---

