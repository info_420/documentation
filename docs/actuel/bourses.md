# Bourses

Les bourses suivantes te sont offertes. À toi de t'inscrire!

[Aide financière](https://www.cegepdrummond.ca/aide-financiere/)

[Aide financière d'urgence](https://www.cegepdrummond.ca/aide-financiere-durgence/)
