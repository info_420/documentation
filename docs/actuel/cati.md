# Le CATI et le EASI

## CATI
Le CATI est le Centre d'Aide pour les Techniques de l'Informatique. C'est le local situé à l'entrée du département au 1325. Il est là pour t'offrir un local pour socialiser avec les autres étudiants de la technique ainsi qu'un milieu de travail pour tes projets d'informatique ou toute autre matière. 

## EASI
Le EASI (Évènements et Activités Sociales en Informatique) est un regroupement d'étudiants de la technique qui s'occupe de l'organisation d'activités sociales tels que des soirées jeux vidéo, des lan party, des activités sportives (volleyball, ballon chasseur... ). Ces activités sont très utiles pour te permettre de t'intégrer et de connaitre les autres étudiants. 

Si tu veux participer, n'hésites pas à rejoindre l'équipe. 

L'équipe pour l'hiver 2024
* Président.e: Sarah Robert
* Vice Président.e : Antoine Gingras
* Trésorier.e: Kevin Ricard
* Membre : Anthony Doyon


