# Documents à connaitre


## Guide de rédaction des travaux

Si tu as un travail à remettre dans un de tes cours d'informatique, voici le guide de rédaction. 

[lien vers pdf](../Normes_presentation_aout_2022.pdf)

## Règlement pédagogique

Le Règlement pédagogique est un outil d’information et un cadre de référence concernant les exigences scolaires du Cégep de Drummondville et l’organisation des activités d’apprentissage. Il présente les règles et les procédures entourant certaines opérations relatives au cheminement des étudiantes et des étudiants et complète la Politique institutionnelle d’évaluation des apprentissages (PIEA).

[sur le portail du cégep](https://www.cegepdrummond.ca/wp-content/uploads/2024/07/REG_03_Reglement_pedagogique_2019-03-26-1.pdf)

## PIÉA

La Politique Institutionnelle de l'Évaluation des Apprentissages contient les règlements du cégep de drummondville en égard à l'évaluation des étudiants durant leur apprentissage. 
[PIÉA sur le portail du cégep](https://www.cegepdrummond.ca/wp-content/uploads/2024/07/POL_01_PIEA_2017_11_28.pdf)

## PDÉA

La Politique Départementale de l'Évaluation des Apprentissages contient les règlements spécifiques aux département d'informatique du cégep de drummondville. Ce document est un prolongement de la PIEA

[PDÉA sur le portail du cégep](https://cegepdrummond.omnivox.ca/intr/webpart.gestion.consultation/Informatique_PDEA_2024.pdf?IdWebPart=0A03582C-A722-420A-8F34-747E761AC09A&idDoc=OTY0OGQzZmQtN2YxMC00ODMxLTk0ZWItYzRjZjA4ZTQyMDAy&from=accueil%22)