import { defineConfig } from 'vitepress'
import { tabsMarkdownPlugin } from 'vitepress-plugin-tabs'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Documentation Info 420",
  description: "Documentation du département d'informatique",
  outDir: "../public",
  base: "/documentation",
  markdown: {
    config(md) {
      md.use(tabsMarkdownPlugin) // ca donne une erreur mais ca fonctionne !
    }
  },
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    docFooter: {
      prev: "Page précédente",
      next: "Page suivante"
    },
    outline:{
      label: "Sur cette page"
    },
    search: {
      provider: 'local'
    },
    nav: [
      { text: 'Accueil', link: '/' },
      { text: 'Cégep', link: 'https://www.cegepdrummond.ca/' }
    ],

    sidebar: [
        {
          text: 'Etudiant.e.s actuels',
          collapsed: true,
          items: [
            { text: 'Accueil', link: '/actuel/' },
            { text: 'Cheminement', link: '/actuel/cheminement' },
            { text: 'Services offert', link: '/actuel/servicesCegep' },
            { text: 'Les bourses d\'étude', link: '/actuel/bourses' },
            { text: 'Documents', link: '/actuel/documents'},
            { text: 'CATI/EASI', link: '/actuel/cati'},

          ]
        },
        {
          text: 'Stage',
          collapsed: true,
          items: [
            { text: 'Accueil', link: '/stages/' },            
            { text: 'Documents de stage', link: '/stages/documentsStages' },

            { text: 'Lieux de stages pour 2025', link: '/stages/lieuxDeStage2025' },
          ]
        },
        {
          text: 'International',
          collapsed: true,
          items: [
            { text: 'Accueil', link: '/international/' },
            { text: 'Le clavier', link: '/international/clavier' },

          ]
        },

        {
          text: 'Stage ATE',
          collapsed: true,
          items: [
            { text: 'Accueil', link: '/stagesATE/' },
          ]
        },
        {
          text: 'Portable',
          collapsed: true,
          items: [
            { text: 'Accueil', link: '/portable/' },
            { text: 'Les touches utiles', link: '/portable/touches' },
            { text: 'Organiser tes projets', link: '/portable/fichiers' },
            { text: 'Les logiciels', link: '/portable/logiciels' },


          ]
        },
        {
          text: 'Plagiat',
          collapsed: true,
          items: [
            { text: 'Accueil', link: '/plagiat/' },
          ]
        },
        {
          text: 'Programmation',
          collapsed: true,
          items: [
            { text: 'Accueil', link: '/programmation/' },
          ]
        },
      
      ],
    

    socialLinks: [
      { icon: 'facebook', link: 'https://www.facebook.com/groups/ancien.informatique.drummond' }
    ],
    


  }
})
