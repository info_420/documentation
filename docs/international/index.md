# Étudiants et étudiantes internationaux

Tu viens d'arriver au Québec, voici quelques informations qui pourraient t'intéresser. 


[Site du cégep pour les étudiants internationaux](https://www.cegepdrummond.ca/international/)

Responsable de l'accueil des étudiants internationaux: **Karine Bélanger**, tél: 819-478-4671 **x4529**