# Les stages Alternances Travail Étude

Il est possible de faire un stage rémunéré durant l'été après ta première et deuxième année d'étude. 

Ces stages sont gérés par la formation continue du Cégep. 

[Page du cégep](https://www.cegepdrummond.ca/inc-recrutement-de-main-doeuvre-etudiante/)