# La programmation 

Dans cette section tu trouveras de l'information sur les bonnes pratiques recommandées dans tes cours de programmation. 

## Comment nommer les variables

Voici quelques règles commune à la majorité des langages enseignés dans tes cours. Tu trouveras les règles spécifiques à chacun des langages dans le tableau ci-bas. 

* Le nom ne doit **pas** **commencer** par un **chiffre**

* Le nom **peut** contenir
  * des caractères de **A à Z** (**majuscule** et **minuscule**, pas d'accent)
  * des **chiffres**
  * le symbole de **soulignement** (`_`)
    * notez que le `_` en début de nom est valide, mais à un signification particulière dans plusieurs langage. Il est donc à utiliser uniquement si vous comprenez sa signification. 
* Il n'est pas recommandé d'utiliser des accents dans un nom de variable. Cela peut causer des problèmes lors du transfert du programme à un autre usager. Il se peut que ca fonctionne, comme il se peut que non... pourquoi prendre la chance! 
* Le nom doit être **significatif**
* Le nom doit être de préférence **court** (sauf si cela le rend moins significatif)
* Le nom ne doit **pas** être un **mot réservé** du langage
* Le nom doit être en **français** :)

* Le nom distingue la variable des autres. Il est donc important d'avoir un nom qui est facile à lire et qui ne causera pas de confusion. 

    * Par exemple, une variable ayant un nom avec un l (la lettre) et une autre ayant pratiquement le même nom mais avec un 1 (le chiffre un ) seront difficiles à différencier (même chose pour O (la lettre o majuscule) et le chiffre 0 (zéro))

* Il est fréquent d'utiliser un variable ayant un nom au singulier lorsqu'on parle d'une seule valeur, et de prendre le même nom au pluriel (simplement en ajoutant un s dans plusieurs des cas) lorsqu'on parle de plusieurs de ces choses (une liste, ou un tableau)
  
Pour chacun des langages suivants, les termes suivants seront utilisés:

* **camelCase**
    * première lettre du premier mot en minuscule
    * première lettre des autres mots en majuscule
    * les autres lettres en minuscule

* **PascalCase**
    * première lettre du premier mot en majuscule
    * première lettre des autres mots en majuscule
    * les autres lettres en minuscule
* **snake_case** ou **SNAKE_CASE**
    * tout en minuscule (ou majuscule pour les constantes dans certains langages)
    * les mots étant séparés par le souligné **_**

* Certains langages sont sensibles à la casse (majuscule/minuscule), d'autres non. Il faut donc vérifier pour le langage que vous utilisez. 

:::tabs key:langage
== java

La liste complète des conventions peut-être trouvée sur https://www.oracle.com/java/technologies/javase/codeconventions-contents.html

* Le nom peut aussi comporter un **signe de dollar** (`$`), mais ce n'est pas recommandé. 
* Par convention, utiliser le standard **camelCase**
  * sauf pour les **constantes** 
    * `JE_SUIS_UNE_CONSTANTE`
    * `final double PI = 3.1416`

* Les mots réservés pour Java: https://www.javatpoint.com/java-keywords

* Le nom est **sensible à la casse**


Voici quelques exemples de noms valides:

```java 
int nombreEtudiants;
boolean estFini;
String phrase;
```

des noms qui sont valides, mais très limite:

```java
int w; //bien que valide, que représente w ? le nom n'est peut-être pas significatif. 
int _uneValeur; //il n'est pas recommandé de débuter un nom de variable par _
int une_valeur; //il n'est pas recommandé d'utiliser le _ pour séparer les mots. Mettez des majuscules. 
int Unnomdevariable; //ne respecte pas le camelCase, ce qui le rend difficile à lire
int theBestName; //en anglais
int UN_NOM_DE_VARIABLE; //un nom tout en majuscule indique que ca devrait être une constante. 


```
Et des noms invalides  

```java
int 123abc; //commence par une lettre
int abc-xyz; //le - n'est pas valide
int _; //le _ ne peut être seul 
```

== c#
```csharp

c#

```

== python
```python 

python

```


== eiffel
```eiffel 

eiffel

```


:::

## Documentation des fonctions

:::tabs key:langage
== java
js 2


== c#
c# 2



== python
```python 

python

```
== eiffel
```eiffel 

eiffels

```
:::



## Documentation des classes
:::tabs key:langage
== java
js 2


== c#
c# 2



== python
```python 

python

```
== eiffel
```eiffel 

eiffels

```
:::

