# La gestion des fichiers sur ton portable

Durant tes études, tu vas réaliser des dizaines de projets. Il est important de bien organiser ton ordinateur afin de pouvoir t'y retrouver. Voici quelques recommandations, 

## Structure de répertoires

Nous te recommandons fortement de créer un répertoire directement à la racine de ton disque C: et d'y créer un répertoire pour chacun de tes cours. 

```
C:/projets/
    1N1/
        projet1/
        projet2/
    1W1/
        projetweb1/
        projetweb2/
    
```
