# Les touches du clavier

Ton clavier est un outil essentiel pour coder. Il est donc important que tu le maitrise et que tu sois capable d'y trouver toutes les touches utilisées dans les différents langages de programmation que tu utiliseras tout au long de tes études. 

Nous avons créé un site expliquant la majorité des touches spéciales que tu devras connaitre durant tes études. 

https://fsthilaire.github.io/aide-clavier/


Si vous ne trouvez pas une ou plusieurs touches indiquées dans ce document, il serait important de demander de l'aide pour la localiser car vous en aurez besoin tout au long de vos études. 

Vous devez aussi comprendre quelques touches du clavier:

- Enter/Return/Entré/Retour de chariot
- Esc/Escape
- Shift/Majuscule
- Ctrl/Contrôle
- Alt

![le clavier](/clavier.png)

Pour copier/coller rapidement:  `ctrl-c` pour copier, suivit de `ctrl-v` pour coller

Pour sélectionner tout le texte d'une page: `ctrl-a`

Pour déplacer du texte (**couper**/coller): `ctrl-x, ctrl-v` 
