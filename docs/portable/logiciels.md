# Logiciels utilisés dans plusieurs cours

Durant tes études, tu utiliseras plusieurs logiciels afin de réaliser les projets demandés dans tes cours. Tu trouveras ici un appercu de certains d'entre eux. 

::: info
Il est possible que la procédure d'installation soit différente d'un cours à l'autre. Dans ce cas, celle de ton cours a priorité. 
:::

## VPN

Si tu désires travailler de chez toi et accéder à certaines informations se trouvant sur le serveur départemental, il te faudra un VPN. 

Pour le télécharger: https://notes-2n3.robinhoodsj.org/_media/vpn.msi

Pour les instructions d'installation et d'utilisation: https://notes-2n3.robinhoodsj.org/_media/vpn.pdf


## notepad++

Notepad++ est un éditeur gratuit, rapide et polyvalent pour Windows. 

Pour le télécharger: https://notepad-plus-plus.org/downloads/

## vs code

VS code, ou Visual Studio Code, est un autre éditeur de code puissant, facile d'utilisation, et compatible avec plusieurs des langages qui seront utilisés durant tes études. 

Pour le télécharger: https://code.visualstudio.com/


### Git

Git est un gestionnaire de version permettant la collaboration entre plusieurs développeurs. Nous allons utiliser cet outil pour que je puisse vous partager du code.

Pour le télécharger: https://gitforwindows.org

Faites l'installation avec les options par défaut


## 7zip

Permet de compresser un ou plusieurs fichiers en 7z ou en ZIP. Ce sera utile pour remettre des travaux et décompresser certains fichiers distribués par les enseignants.

Pour le télécharger: https://www.7-zip.org/

Pour Windows, prenez la version 64-bit x64

Installez 7zip avec les options par défaut


::: danger Attention
Il ne faut pas simplement faire un double clic sur un fichier .zip ou 7z. Il faut en extraire le contenu. Si vous faites un double clic, le contenu du fichier sera affiché, mais ce contenu est compressé et difficilement accessible. 
:::


## Discord

Discord est un logiciel de chat surtout utilisé par les gamers. Nous l'utilisons afin de permettre une communication instantanée entre les étudiant.e.s, les enseignant.e.s, et même les anciens étudiant.e.s.

Vous pouvez le télécharger sur https://discord.com et rejoindre notre serveur **420DM**.