---
 title: tx
 numbersections: true
---

# Documents pour les stages


## Mandat de stage

Ce document est la première étape pour obtenir un stage. Tu dois le faire remplir par un responsable dans l'entreprise à laquelle tu désires faire ton stage. Elle permettra au superviseur de stage du cégep d'avoir une idée du stage proposé et ainsi pouvoir décider si le stage est valide ou non. 

[Mandat de stage](/MandatDeStage.zip)

## Guide de stage - cahier de l'étudiant

 Ce guide de stage s’adresse aux stagiaires, aux milieux de stage et aux superviseurs qui accompagnent les stagiaires en informatique. Il a pour but de les renseigner sur les diverses modalités du stage et d’en préciser les objectifs conformément aux politiques du département de Techniques de l’informatique du Cégep de Drummondville.

[Guide de l'étudiant](/Guide%20de%20stage%20-%20cahier%20de%20l'étudiant.pdf)


## Guide de stage - cahier du responsable en entreprise

Ce guide de stage s’adresse aux responsables en milieu de stage et il a pour but de les renseigner sur les diverses modalités du stage et d’en préciser les objectifs conformément aux politiques du département de Techniques de l’informatique du Cégep de Drummondville.

[Guide du responsable en entreprise](/Guide%20de%20stage%20-%20cahier%20du%20responsable%20en%20milieu%20de%20stage.pdf)


## Entente de service

L'entente de service est à remettre par l'étudiant au responsable de l'entreprise en début de stage. Elle permet de formaliser l'engagement pris par l'entreprise envers l'étudiant. 

[Entente de service](/EntenteDeService.zip)