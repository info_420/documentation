# Stages

À la dernière session de tes études, tu réaliseras un stage en entreprise. Cette section contient tous les documents pour t'aider à effectuer cette étape importante de tes études. 

La formation en Techniques de l’informatique ne saurait être complète sans cette expérience de stage réalisée auprès d’une institution du secteur privé ou du secteur public. L’environnement de stage permettra au stagiaire de mettre en pratique l’enseignement reçu ainsi que les compétences spécifiques à sa profession dans un milieu de travail stimulant.
